using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public float x, z;
    private Canvas canvas;
    private void Start()
    {
        x=5f; 
        z=0;
        canvas = GetComponent<Canvas>();
        if (canvas.name.Equals("CanvasPause") )
        {
            canvas.enabled = false;
        }
        if (canvas.name.Equals("CanvasWinLose"))
        {
            canvas.enabled = false;
        }

    }
    public void ToggleGame()
    {
        SceneManager.LoadScene("game"); 
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("StartMenu");
    }
    public void Quit()
    {
        Application.Quit();
    }

    public void PauseGame()
    {
        GameObject.Find("CanvasPause").GetComponent<Canvas>().enabled = true;
        Time.timeScale = 0f;
    }
    public void ResumeGame()
    {
        GameObject.Find("CanvasPause").GetComponent<Canvas>().enabled = false;
        Time.timeScale = 1f;
    }

    public void Right()
    {
        x = 5f;
        z = 0;
    }
    public void Left()
    {
        x = -5f;
        z = 0;
    }
    public void Up()
    {
        z = 5f;
        x = 0;
    }
    public void Down()
    {
        z = -5f;
        x = 0;
    }
}
