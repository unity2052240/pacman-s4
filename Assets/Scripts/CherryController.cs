using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CherryController : MonoBehaviour
{
    float timer;
    float timertomove;
    Rigidbody rb;
    private bool up;
    private Collider col;
    // Start is called before the first frame update
    void Start()
    {
        timertomove = 1f;
        up = true;
        col = GetComponent<Collider>();
        rb = GetComponent<Rigidbody>();
        timer = 10f;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(up);
        timertomove -= Time.deltaTime;
        if (timertomove < 0)
        {
            timertomove =1f;
            up = !up;
        }
        if (up)
        {
            rb.velocity = new Vector3(0, 2f, 0);
        }
        else if(!up)
        {
            rb.velocity = new Vector3(0, -2f, 0);
        }
        
        timer -= Time.deltaTime;
        if(timer < 0 )
        {
            Destroy( gameObject );
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.name.StartsWith("Coin"))
        {
            col.isTrigger = true;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name.StartsWith("Coin"))
        {
            col.isTrigger = false;
        }
    }
}
