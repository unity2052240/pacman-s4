using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using static UnityEngine.GraphicsBuffer;

public class Redcontroller : MonoBehaviour
{
    private Rigidbody rb;
    public Transform tr;
    public NavMeshAgent agent;
    public Material normalmat;
    public Material fearmat;
    private GameObject player;
    private PlayerControl playerControl;


    // Start is called before the first frame update
    void Start()
    {
        /*go = GameObject.Find("Sphere");*/
        rb = GetComponent<Rigidbody>();
        player = GameObject.Find("Sphere");
        playerControl = player.GetComponent<PlayerControl>();

    }

    // Update is called once per frame
    void Update()
    {
        if (playerControl.poweractive == true)
        {
            gameObject.GetComponent<SkinnedMeshRenderer>().material = fearmat;
        }
        else
            gameObject.GetComponent<SkinnedMeshRenderer>().material = normalmat;
        /*if(playerControl.poweractive==false)
        {
            agent.destination = tr.position;
        }
        
        else if (playerControl.poweractive == true)
        {
            Vector3 directionToFlee = transform.position - tr.position;
            agent.destination = transform.position + directionToFlee;
        }*/
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.name.Equals("Wall1"))
        {
            transform.position = new Vector3(7.5f, 0f, -19.6f);
        }
        else if (collision.gameObject.name.Equals("Wall2"))
        {
            transform.position = new Vector3(6.9f, 0f, 19.7f);
        }
        else if (collision.gameObject.name.Equals("Sphere"))
        {
            Destroy(gameObject);
            Destroy(GameObject.Find("Green(Clone)"));
        }

    }
}
