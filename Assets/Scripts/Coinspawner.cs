using UnityEngine;

public class Coinspawner : MonoBehaviour
{
    public GameObject coinPrefab;
    public GameObject walls;
    public GameObject green;
    public GameObject red;
    public GameObject pwerup;
    public GameObject player;
    public GameObject cherry;
    private int randomgreen;
    private int randomcherry;
    private int randomred;
    private float timercherry;

    void Start()
    {
        timercherry = 10f;
        Instantiate(walls, new Vector3(34.65928f, -0.75f, 6.6595f), Quaternion.identity);
        Instantiate(pwerup, new Vector3(32.6f, 0.26f, 19.5f), Quaternion.identity);
        Instantiate(pwerup, new Vector3(-20.6f, 0.26f, 19.6f), Quaternion.identity);
        Instantiate(pwerup, new Vector3(-20.2f, 0.26f, -19f), Quaternion.identity);
        Instantiate(coinPrefab,coinPrefab.transform.position, Quaternion.identity);
        Instantiate(player, player.transform.position, Quaternion.identity);
        randomred = Random.Range(1, 4);
        randomgreen = Random.Range(1, 4);
        if(!GameObject.Find("Red(Clone)"))
        switch (randomred)
        {
            case 1:
                Instantiate(red, new Vector3(28.5f, -0.54f, -17.6f), Quaternion.identity);
                break;
            case 2:
                Instantiate(red, new Vector3(31.9f, -0.54f, 12.2f), Quaternion.identity);
                break;
            case 3:
                Instantiate(red, new Vector3(0.6f, -0.54f, -6.5f), Quaternion.identity);
                break;
        }
        if(!GameObject.Find("Green(Clone)"))
        switch (randomgreen)
        {
            case 1:
                Instantiate(green, new Vector3(-19.3f, -0.54f, 12.6f), Quaternion.identity);
                break;
            case 2:
                Instantiate(green, new Vector3(-19.3f, -0.54f, -19f), Quaternion.identity);
                break;
            case 3:
                Instantiate(green, new Vector3(32.4f, -0.54f, 19.5f), Quaternion.identity);
                break;
        }

    }
    private void Update()
    {
        randomcherry =Random.Range(1, 4);
        randomred = Random.Range(1, 4);
        randomgreen = Random.Range(1, 4);
        if (!GameObject.Find("Red(Clone)"))
            switch (randomred)
            {
                case 1:
                    Instantiate(red, new Vector3(28.5f, -0.54f, -17.6f), Quaternion.identity);
                    break;
                case 2:
                    Instantiate(red, new Vector3(31.9f, -0.54f, 12.2f), Quaternion.identity);
                    break;
                case 3:
                    Instantiate(red, new Vector3(0.6f, -0.54f, -6.5f), Quaternion.identity);
                    break;
            }
        if (!GameObject.Find("Green(Clone)"))
            switch (randomgreen)
            {
                case 1:
                    Instantiate(green, new Vector3(-19.3f, -0.54f, 12.6f), Quaternion.identity);
                    break;
                case 2:
                    Instantiate(green, new Vector3(-18.1f, -0.54f, -19f), Quaternion.identity);
                    break;
                case 3:
                    Instantiate(green, new Vector3(28.7f, -0.54f, 19.5f), Quaternion.identity);
                    break;
            }

        if (!GameObject.Find("Cherry(Clone)"))
        {
            timercherry -= Time.deltaTime;
            Debug.Log(timercherry);
            if(timercherry < 0)
            {
                timercherry = 10f;
                switch (randomcherry)
                {
                    case 1:
                        Instantiate(cherry, new Vector3(27.9f, -0.4f, -6f), Quaternion.identity);
                        break;
                    case 2:
                        Instantiate(cherry, new Vector3(-10.85f, -0.4f, 0.51f), Quaternion.identity);
                        break;
                    case 3:
                        Instantiate(cherry, new Vector3(7.12f, -0.4f, 18.46f), Quaternion.identity);
                        break;
                }
            } 
        }

    }
}
