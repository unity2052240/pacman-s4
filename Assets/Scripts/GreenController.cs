using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.XR;
using Random = UnityEngine.Random;

public class GreenController : MonoBehaviour
{
    private GameObject go;
    private Rigidbody rb;
    float randomX;
    float randomZ;
    private Collider col;
    public Material normalmat;
    public Material fearmat;
    public Transform tr;
    public NavMeshAgent agent;

    private GameObject player;
    private PlayerControl playerControl;

    // Start is called before the first frame updat

    // Start is called before the first frame update
    void Start()
    {
        randomX = Random.Range(0, 2) == 0 ? -4f : 4f;
        randomZ = Random.Range(0, 2) == 0 ? -4f : 4f;
        go = GameObject.Find("Sphere");
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
        player = GameObject.Find("Sphere");
        playerControl = player.GetComponent<PlayerControl>();
    }

    // Update is called once per frame
    void Update()
    {
        if (playerControl.poweractive == true)
        {
            gameObject.GetComponent<SkinnedMeshRenderer>().material= fearmat;
        }
        else
            gameObject.GetComponent<SkinnedMeshRenderer>().material = normalmat;
        /*if (playerControl.poweractive == false)
        {
            col.isTrigger = false;
            if (go != null && (go.GetComponent<Rigidbody>().position - rb.position).magnitude <= 12.0f)
            {

                agent.destination = tr.position;
            }
            else
            {
                agent.destination = new Vector3(randomX, 0, randomZ);
            }
        }

        else if (playerControl.poweractive == true)
        {
            Vector3 directionToFlee = transform.position - tr.position;
            agent.destination = transform.position + directionToFlee;
        }*/
    }

    private void OnCollisionStay(Collision collision)
    {
        if (playerControl.poweractive == false)
        {
            if (collision.gameObject.name.StartsWith("Cube "))
            {
                if (rb.velocity.x > 0)
                {
                    randomZ = Random.Range(0, 2) == 0 ? -4f : 4f;
                    randomX = Random.Range(0, 2) == 0 ? -4f : rb.velocity.x;
                }
                else if (rb.velocity.x < 0)
                {
                    randomZ = Random.Range(0, 2) == 0 ? -4f : 4f;
                    randomX = Random.Range(0, 2) == 0 ? 4f : rb.velocity.x;
                }
                else if (rb.velocity.z > 0)
                {
                    randomX = Random.Range(0, 2) == 0 ? -4f : 4f;
                    randomZ = Random.Range(0, 2) == 0 ? -4f : rb.velocity.z;
                }
                else if (rb.velocity.z < 0)
                {
                    randomX = Random.Range(0, 2) == 0 ? -4f : 4f;
                    randomZ = Random.Range(0, 2) == 0 ? 4f : rb.velocity.z;
                }

            }
        }

        if (collision.gameObject.name.Equals("Wall1"))
        {
            transform.position = new Vector3(7.5f, 0f, -19.6f);
        }
        else if (collision.gameObject.name.Equals("Wall2"))
        {
            transform.position = new Vector3(6.9f, 0f, 19.7f);
        }

        if (collision.gameObject.name.Equals("Red(Clone)"))
        {
            col.isTrigger = true;
        }
        if (collision.gameObject.name.Equals("Sphere"))
        {
                Destroy(gameObject);
                Destroy(GameObject.Find("Red(Clone)"));
        }
    }
}
