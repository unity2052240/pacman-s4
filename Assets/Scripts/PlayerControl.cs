using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class PlayerControl : MonoBehaviour
{
    private Rigidbody rb;
    private Collider col;
    public Vector3 h;
    public Vector3 v;
    [SerializeField]
    public int score;
    [SerializeField]
    public int lives;
    [SerializeField]
    public bool poweractive;
    private float countdownTime;

    void Start()
    {
        Time.timeScale = 1f;
        h.x = 5f; ; v.z = 0f; ;
        transform.position = new Vector3(13.4f, 0, 0.2f);
        transform.rotation = Quaternion.Euler(0, 90, 0);
        rb = GetComponent<Rigidbody>();  
        col = GetComponent<Collider>();
        poweractive = false;
        countdownTime = 10.0f;
    }
    void Update()
    {
        if (rb != null)
        {
            rb.velocity = new Vector3(h.x, 0, v.z);
            col.isTrigger = false;

            if(poweractive==true)
            {
                if (countdownTime > 0)
                {
                    countdownTime -= Time.deltaTime;
                    countdownTime = Mathf.Max(0, countdownTime);
                    GameObject.Find("PwrUpCd").GetComponent<Text>().text = "Power Up: " + countdownTime.ToString("F2") + " seconds";
                }
                else
                {
                    poweractive = false;
                    GameObject.Find("PwrUpCd").GetComponent<Text>().text = "";
                    countdownTime = 10.0f;
                }
            }

            if(Input.GetButtonDown("Horizontal"))
            {
                if (Input.GetAxis("Horizontal")<0)
                {
                    transform.rotation = Quaternion.Euler(0, -90, 0);

                    h.x = -5f;
                }
                else
                {
                    transform.rotation = Quaternion.Euler(0, 90, 0);
                    h.x = 5f;
                }
               
                v.z = 0f;
                
            }
            if (Input.GetButtonDown("Vertical"))
            {

                if (Input.GetAxis("Vertical") < 0)
                {
                    transform.rotation = Quaternion.Euler(0, 180, 0);

                    v.z = -5f;
                }
                else
                {
                    transform.rotation = Quaternion.Euler(0, 0, 0);
                    
                    
                    v.z = 5f;
                }

                h.x = 0f;

            }
            
            GameObject[] allObjects = GameObject.FindObjectsOfType<GameObject>();

            bool foundCoin = false;

            foreach (GameObject obj in allObjects)
            {
                if (obj.name.StartsWith("Coin") || obj.name.Equals("PowerUp(Clone)"))
                {
                    foundCoin = true;
                    break;
                }
            }

            if (foundCoin==false)
            {
                if (GameObject.Find("Sphere") != null && GameObject.Find("Green(Clone)") != null && GameObject.Find("Red(Clone)") != null)
                {
                    Destroy(GameObject.Find("Sphere"));
                    Destroy(GameObject.Find("Green(Clone)"));
                    Destroy(GameObject.Find("Red(Clone)"));
                }

                Debug.Log("YOU WIN!!!!");
                GameObject.Find("WinLose").GetComponent<Text>().text = "YOU WIN!!!";
                GameObject.Find("CanvasWinLose").GetComponent<Canvas>().enabled = true;    
            }

            if (lives==0)
            {
                if (GameObject.Find("Sphere") != null && GameObject.Find("Green(Clone)") != null && GameObject.Find("Red(Clone)") != null)
                {
                    Destroy(GameObject.Find("Sphere"));
                    Destroy(GameObject.Find("Green(Clone)"));
                    Destroy(GameObject.Find("Red(Clone)"));
                }
                Debug.Log("GAME OVER!!!!");
                GameObject.Find("WinLose").GetComponent<Text>().text = "GAME OVER!!!";
                GameObject.Find("CanvasWinLose").GetComponent<Canvas>().enabled = true;
            }
        }
    } 

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.name.StartsWith("Cube "))
        {
            Vector3 zero = Vector3.zero;
            rb.velocity = zero;
        }

        if (collision.gameObject.name.Equals("Wall1"))
        {
            transform.position = new Vector3(7.5f, 0f, -19.6f);
            rb.velocity = new Vector3(h.x, 0, v.y);
        }
        else if (collision.gameObject.name.Equals("Wall2"))
        {
            transform.position = new Vector3(6.9f, 0f, 19.7f);
            rb.velocity = new Vector3(h.x, 0, v.y);
        }

        if (collision.gameObject.name.Equals("Green(Clone)"))
        {
            if (poweractive == false)
            {
                lives -= 1;
                Start();
            }
            else if (poweractive == true)
            {
                score += 100;
            }
        }

        if (collision.gameObject.name.Equals("Red(Clone)"))
        {
            if (poweractive==false)
            {
                lives -= 1;
                Start();
            }
            else if (poweractive == true)
            {
                score += 100;
            }   
        }
        if (collision.gameObject.name.StartsWith("Coin"))
        {
            score += 1;
        }

        if (collision.gameObject.name.StartsWith("PowerUp"))
        {
            poweractive = true;
            score += 50;
        }

        if (collision.gameObject.name.StartsWith("Cherry(Clone)"))
        {
            score += 200;
            Destroy(GameObject.Find("Cherry(Clone)"));
        }
    }
}
