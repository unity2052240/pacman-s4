using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class score : MonoBehaviour
{
    private GameObject player;
    private GameObject go;
    private Text textMesh;
    private PlayerControl playerControl;
    // Start is called before the first frame update
    void Start()
    {
        textMesh = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        player = GameObject.Find("Sphere");
        playerControl = player.GetComponent<PlayerControl>();
        textMesh.text ="Score : " + playerControl.score.ToString();

    }
}
